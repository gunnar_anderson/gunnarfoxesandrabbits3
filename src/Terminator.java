import java.util.Iterator;
import java.util.Random;

public class Terminator extends Actor {

	public Terminator() {
		super();
	}

	public void act(Field currentField, Field updatedField)
    {
 
            // Move towards the source of food if found.
            Location newLocation = terminate(currentField, getLocation());
            if(newLocation == null) {  // no food found - move randomly
                newLocation = updatedField.freeAdjacentLocation(getLocation());
            }
            if(newLocation != null) {
                setLocation(newLocation);
                updatedField.place(this);  // sets location
            }
            else {
                setLocation(getLocation());
            }
        }
	
	 private Location terminate(Field field, Location location)
	    {
	        Iterator<Location> adjacentLocations =
	                          field.adjacentLocations(location);
	        while(adjacentLocations.hasNext()) {
	            Location where = adjacentLocations.next();
	            Actor actor = field.getActorAt(where);
	            if(actor instanceof Rabbit) {
	                Rabbit rabbit = (Rabbit) actor;
	                if(rabbit.isAlive()) { 
	                    rabbit.setDead();
	                   
	                    return where;
	                }
	                    else if(actor instanceof Fox) {
	        	                Fox fox = (Fox) actor;
	        	                if(fox.isAlive()) { 
	        	                    fox.setDead();
	        	                   
	        	                    return where;
	                    	
	                    }
	                }
	            }
	        }
	        return null;
	    }
	
	
    }
	
	

